#!/usr/bin/env python3

import argparse
import functools
import json
import logging
import pathlib


# based on https://stackoverflow.com/a/7205107/65624
def merge_dicts(a, b, _path=None):
    if _path is None:
        _path = []
    for key in b:
        if key not in a:
            a[key] = b[key]
        elif a[key] == b[key]:
            pass  # same leaf value
        elif isinstance(a[key], dict) and isinstance(b[key], dict):
            merge_dicts(a[key], b[key], _path + [str(key)])
        elif isinstance(a[key], list) and isinstance(b[key], list):
            a[key].extend(b[key])
        else:
            raise Exception(f'Conflict at {".".join(_path + [str(key)])}')
    return a


def load_json(f):
    logging.info(f"Loading JSON from {f.name}")
    with open(f.name) as d:
        data = json.load(d)
    return data


def merge(inputs):
    merged = functools.reduce(merge_dicts, inputs)
    return merged


def is_fresh_enough(inputs, output):
    latest_ts = max(pathlib.Path(i).stat().st_mtime for i in inputs)
    outfile = pathlib.Path(output)
    outfile_ts = outfile.stat().st_mtime if outfile.exists() else None
    logging.debug(
        f"The {output} file has timestamp {outfile_ts}, the latest input has {latest_ts}"
    )
    return outfile_ts and latest_ts < outfile_ts


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Deep merge JSON files, mixing dict entries and appending to arrays"
    )
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )
    parser.add_argument(
        "--log-to-file",
        action="store",
        type=str,
        dest="log_to_file",
        help="Log all output to file",
    )
    parser.add_argument(
        "--input",
        required=True,
        action="append",
        type=argparse.FileType("r"),
        help="input file in JSON format",
    )
    parser.add_argument(
        "--output", required=True, type=str, help="output file in JSON format"
    )
    parser.add_argument(
        "--output-cache",
        type=str,
        dest="output_cache",
        help="output cache file in JSON format",
    )
    args = parser.parse_args()

    if args.log_to_file:
        logging.basicConfig(
            level=args.loglevel or logging.INFO, filename=args.log_to_file
        )
    else:
        logging.basicConfig(level=args.loglevel or logging.INFO)

    if is_fresh_enough([i.name for i in args.input], args.output):
        logging.info(f"The {args.output} file is already up-to-date, nothing to do")
    else:
        logging.debug(f"Merging {len(args.input)} items")
        inputs = (load_json(f) for f in args.input)
        data = merge(inputs)
        with open(args.output, "w") as f:
            json.dump(data, f)
        if args.output_cache:
            logging.debug("Generating cache file")
            data_pkgs = data["packages"]
            cache = {
                "packages": {
                    key: value for key, value in data_pkgs.items() if "git" in value
                }
            }
            with open(args.output_cache, "w") as f:
                json.dump(cache, f)
