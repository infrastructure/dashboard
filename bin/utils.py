import collections.abc
import concurrent.futures
import logging
import traceback

import tenacity


def _retrying_on_exception(timeout, exception_type=Exception):
    retrying = tenacity.Retrying(
        stop=tenacity.stop_after_delay(timeout),
        retry=tenacity.retry_if_exception_type(exception_type),
        wait=tenacity.wait_exponential_jitter(max=timeout / 10),
    )
    return retrying


def item_id(item):
    itemid = None
    keys = ("path_with_namespace", "id", "name")
    for k in keys:
        if itemid is None:
            itemid = getattr(item, k, None)
    for k in keys:
        if itemid is None and isinstance(item, collections.abc.Mapping):
            itemid = item.get(k)
    if itemid is None and isinstance(item, str):
        itemid = item
    if itemid is None and isinstance(item, collections.abc.Sequence):
        itemid = tuple(item_id(i) for i in item)
    if itemid is None:
        itemid = item
    return itemid


def thread_pool(num_workers, func, items, *, retryable=False):
    def func_with_retries(item, counter, total):
        itemid = item_id(item)
        for attempt in _retrying_on_exception(600 if retryable else 0):
            i = attempt.retry_state.attempt_number
            logging.debug(f"Fetching item {counter} of {total} ({itemid}), round #{i}")
            with attempt:
                func(item)
            if attempt.retry_state.outcome.failed:
                logging.error(
                    f"Failed processing item {counter} of {total} ({itemid}), round #{i}"
                )
                if logging.DEBUG >= logging.root.level:
                    ex = attempt.retry_state.outcome.exception()
                    traceback.print_exception(type(ex), ex, ex.__traceback__)

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        items = list(items)
        total = len(items)
        futures = [
            executor.submit(func_with_retries, item, counter, total)
            for counter, item in enumerate(items, start=1)
        ]
    results = [f.result() for f in futures]
    return results
