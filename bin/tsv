#!/usr/bin/env python3

import argparse
import csv
import json
import logging
import pathlib


def render(data, destdir):
    destdir.mkdir(parents=True, exist_ok=True)
    for channelname, channel in data["channels"].items():
        suite = channel["suite"]
        tsv_file = destdir / f"{suite}.tsv"
        tsv_data = open(tsv_file, "wt")
        tsv_writer = csv.writer(tsv_data, delimiter="\t")
        tsv_writer.writerow(["Package", "Version", "Component"])
        for packagename, package in sorted(data["packages"].items()):
            published = package.get("published", {}).get(channelname, [])
            sources = [p["source"] for p in published if "source" in p]
            if not sources:
                continue
            s = sources[0]
            tsv_writer.writerow([s["name"], s["version"], s["component"]])
        tsv_data.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Fetch data from the upstream (Debian) APT repositories"
    )
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )
    parser.add_argument(
        "--log-to-file",
        action="store",
        type=str,
        dest="log_to_file",
        help="Log all output to file",
    )
    parser.add_argument(
        "--data",
        required=True,
        type=argparse.FileType("r"),
        help="input file in JSON format",
    )
    parser.add_argument("--destdir", required=True, type=str, help="output directory")
    args = parser.parse_args()

    if args.log_to_file:
        logging.basicConfig(
            level=args.loglevel or logging.INFO, filename=args.log_to_file
        )
    else:
        logging.basicConfig(level=args.loglevel or logging.INFO)

    logging.debug(f"Loading JSON from {args.data.name}")
    data = json.load(args.data)

    render(data, pathlib.Path(args.destdir))
