//! Wraps a reqwest client with handling for retries & compressed files.

use anyhow::{Context, Result};
use backoff::{future::retry, ExponentialBackoff};
use bytes::Bytes;
use flate2::read::GzDecoder;
use lzma_rs::xz_decompress;
use netrc::{Machine, Netrc};
use reqwest::IntoUrl;
use secrecy::{ExposeSecret, SecretString};
use std::{
    collections::HashMap,
    fmt::Debug,
    io::{Cursor, Read},
    sync::Arc,
};
use tokio::sync::Semaphore;
use tracing::{debug, instrument, trace, warn};

pub enum Compression {
    Gzip,
    Lzma,
}

pub struct ClientOptions {
    pub max_parallel: usize,
    pub netrc: Option<Netrc>,
}

#[derive(Clone, Debug)]
struct Credentials {
    username: String,
    password: Option<SecretString>,
}

impl Credentials {
    fn from_machine(machine: Machine) -> Credentials {
        Credentials {
            username: machine.login,
            password: machine.password.map(SecretString::new),
        }
    }
}

#[derive(Clone, Default)]
struct CredentialsMapper {
    default: Option<Credentials>,
    hosts: HashMap<String, Credentials>,
}

impl CredentialsMapper {
    fn from_netrc(netrc: Netrc) -> CredentialsMapper {
        CredentialsMapper {
            default: netrc.default.map(Credentials::from_machine),
            hosts: netrc
                .hosts
                .into_iter()
                .map(|(host, machine)| (host, Credentials::from_machine(machine)))
                .collect(),
        }
    }

    fn lookup(&self, host: &str) -> Option<&Credentials> {
        self.hosts.get(host).or(self.default.as_ref())
    }
}

#[derive(Clone)]
pub struct Client {
    client: reqwest::Client,
    limiter: Arc<Semaphore>,

    credentials: CredentialsMapper,
}

pub fn is_http_client_error(error: &anyhow::Error) -> bool {
    for cause in error.chain() {
        if let Some(req_cause) = cause.downcast_ref::<reqwest::Error>() {
            if let Some(status) = req_cause.status() {
                return status.is_client_error();
            }
        }
    }

    false
}

impl Client {
    pub fn new(options: ClientOptions) -> Client {
        Client {
            client: reqwest::Client::new(),
            limiter: Arc::new(Semaphore::new(options.max_parallel)),
            credentials: options
                .netrc
                .map(CredentialsMapper::from_netrc)
                .unwrap_or_default(),
        }
    }

    async fn get_no_retry(&self, url: reqwest::Url) -> Result<Bytes> {
        let _permit = self.limiter.acquire().await.unwrap();

        // NOTE: the URL should be printed since get() is instrumented.
        debug!("GET");

        let req = if let Some(creds) =
            url.host_str().and_then(|host| self.credentials.lookup(host))
        {
            debug!(?creds, "Using netrc credentials");
            self.client.get(url).basic_auth(
                &creds.username,
                creds.password.as_ref().map(ExposeSecret::expose_secret),
            )
        } else {
            self.client.get(url)
        };

        Ok(req.send().await?.error_for_status()?.bytes().await?)
    }

    #[instrument(skip(self))]
    pub async fn get<U: IntoUrl + Debug>(&self, url: U) -> Result<Bytes> {
        let url = url.into_url().context("Invalid URL")?;

        let result = retry(ExponentialBackoff::default(), || async {
            self.get_no_retry(url.clone()).await.map_err(|err| {
                // Don't treat client errors as transient (because they're usually not +
                // it can be handled in a higher layer to e.g. find the first file
                // available on 404).
                if is_http_client_error(&err) {
                    trace!(?err, "4xx error, not retrying");
                    backoff::Error::Permanent(err)
                } else {
                    warn!(?err, "Transient error querying URL");
                    backoff::Error::Transient(err)
                }
            })
        })
        .await?;

        debug!("GET success");
        Ok(result)
    }

    pub async fn get_compressed<U: IntoUrl + Clone + Debug>(
        &self,
        url: U,
        compression: Compression,
    ) -> Result<Bytes> {
        let bytes = self.get(url).await?;
        let mut buf = vec![];

        match compression {
            Compression::Gzip => {
                let mut gz = GzDecoder::new(&bytes[..]);
                gz.read_to_end(&mut buf)?;
            }
            Compression::Lzma => {
                xz_decompress(&mut Cursor::new(bytes), &mut buf)?;
            }
        }

        Ok(Bytes::from(buf))
    }
}
