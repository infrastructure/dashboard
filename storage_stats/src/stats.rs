//! Computes storage statistics for an Apertis package repository.

use crate::{
    client::Client,
    repository::Repository,
    sources::SourceDefinition,
    usage::{AggregationOptions, StorageUsage, StorageUsageAggregator},
};
use anyhow::{Context, Result};
use futures::{future::try_join_all, stream, try_join, StreamExt, TryStreamExt};
use serde_derive::Serialize;
use std::sync::Arc;
use tracing::{debug, info, instrument, warn};

async fn scan_source_packages(
    client: Client,
    repo: Repository,
    component: String,
    agg: Arc<StorageUsageAggregator>,
) -> Result<()> {
    match repo.read_sources(&client, &component).await? {
        Some(pkgs) => {
            for pkg in pkgs {
                for file in pkg.files {
                    agg.add(
                        &repo.id,
                        format!("{}/{}", pkg.directory, file.filename),
                        file.size,
                    );
                }
            }
        }
        None => {
            warn!(
                "Repository {}, component {} is missing its Sources file",
                repo.id, component
            );
        }
    }

    Ok(())
}

async fn scan_binary_packages(
    client: Client,
    repo: Repository,
    component: String,
    arch: String,
    agg: Arc<StorageUsageAggregator>,
) -> Result<()> {
    match repo.read_packages(&client, &component, &arch).await? {
        Some(pkgs) => {
            for pkg in pkgs {
                agg.add(&repo.id, pkg.filename, pkg.size);
            }
        }
        None => {
            warn!(
                "Repository {}, component {}, arch {} is missing its Packages file",
                repo.id, component, arch
            );
        }
    }

    Ok(())
}

#[instrument(skip_all, fields(repo = ?repo.id))]
async fn aggregate_basic_stats(
    client: &Client,
    repo: &Repository,
    source_def: &SourceDefinition,
    agg: Arc<StorageUsageAggregator>,
) -> Result<()> {
    debug!("Scanning repo");

    let release = match repo.read_release(client).await? {
        Some(release) => release,
        None => {
            warn!("Repository {} is missing its Release file", repo.id);
            return Ok(());
        }
    };

    let metadata_size: usize = release.files.iter().map(|entry| entry.size).sum();

    agg.register_key(repo.id.to_owned(), metadata_size);

    let mut source_futs = vec![];
    let mut binary_futs = vec![];
    for component in &source_def.components {
        source_futs.push(scan_source_packages(
            client.clone(),
            repo.clone(),
            component.to_owned(),
            agg.clone(),
        ));
        for arch in &release.architectures {
            binary_futs.push(scan_binary_packages(
                client.clone(),
                repo.clone(),
                component.clone(),
                arch.clone(),
                agg.clone(),
            ));
        }
    }

    try_join!(try_join_all(source_futs), try_join_all(binary_futs))?;

    debug!(metadata_size, "Finished scan");

    Ok(())
}

#[derive(Debug, Serialize)]
pub struct SnapshotStats {
    pub id: String,
    #[serde(flatten)]
    pub usage: StorageUsage,
}

impl SnapshotStats {
    async fn compute_all(
        client: &Client,
        snapshots: &[Repository],
        source_def: &SourceDefinition,
        max_snapshots: Option<usize>,
    ) -> Result<Vec<SnapshotStats>> {
        let agg = Arc::new(StorageUsageAggregator::new());

        let max_snapshots = max_snapshots.unwrap_or(snapshots.len());
        let all_stats =
            try_join_all(snapshots[0..max_snapshots].iter().map(|snapshot| {
                aggregate_basic_stats(client, snapshot, source_def, agg.clone())
            }))
            .await?;

        assert_eq!(max_snapshots, all_stats.len());

        let result_stats = agg
            .unwrap_collect(AggregationOptions { compute_history: true })
            .into_iter()
            .map(|(id, usage)| SnapshotStats { id, usage })
            .collect();

        Ok(result_stats)
    }
}

#[derive(Debug, Serialize)]
pub struct SuiteStats {
    pub id: String,
    #[serde(flatten)]
    pub usage: StorageUsage,
    #[serde(rename = "snapshots")]
    pub snapshot_stats: Vec<SnapshotStats>,
}

impl SuiteStats {
    #[instrument(skip_all, fields(suite = ?source_def.suite))]
    async fn compute(
        client: &Client,
        source_def: &SourceDefinition,
        max_snapshots: Option<usize>,
        agg: Arc<StorageUsageAggregator>,
    ) -> Result<Vec<SnapshotStats>> {
        info!("Scanning suite");

        let repo = source_def.root_repository();
        aggregate_basic_stats(client, &repo, source_def, agg).await?;

        let snapshot_stats = if let Some(0) = max_snapshots {
            debug!("Skipping snapshots scan because max-snapshots=0");
            vec![]
        } else {
            let snapshots =
                repo.list_snapshots(client).await.context("Failed to list snapshots")?;
            info!("Scanning {} snapshots", snapshots.len());

            SnapshotStats::compute_all(client, &snapshots, source_def, max_snapshots)
                .await?
        };

        info!("Finished scan");
        Ok(snapshot_stats)
    }

    pub async fn compute_all(
        client: &Client,
        source_defs: &[SourceDefinition],
        max_snapshots: Option<usize>,
    ) -> Result<Vec<SuiteStats>> {
        let agg = Arc::new(StorageUsageAggregator::new());

        let all_snapshot_stats: Vec<Vec<SnapshotStats>> =
            stream::iter(source_defs.iter())
                .then(|def| SuiteStats::compute(client, def, max_snapshots, agg.clone()))
                .try_collect()
                .await?;

        assert_eq!(source_defs.len(), all_snapshot_stats.len());

        let result_stats = agg
            .unwrap_collect(AggregationOptions { compute_history: false })
            .into_iter()
            .zip(all_snapshot_stats)
            .map(|((id, usage), snapshot_stats)| SuiteStats { id, usage, snapshot_stats })
            .collect();

        Ok(result_stats)
    }
}
