//! Interacts with a remote Apertis package repository, parsing the files within and
//! listing snapshots.

use crate::{
    client::{is_http_client_error, Client, Compression},
    deb822::{self, FileEntry},
};
use anyhow::{Context, Result};
use bytes::Bytes;
use futures::Future;
use once_cell::sync::Lazy;
use regex::Regex;
use scraper::{Html, Selector};
use serde::de::DeserializeOwned;
use serde_derive::Deserialize;
use tracing::{debug, instrument};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct RepositoryRelease {
    #[serde(deserialize_with = "deb822::space_delimited")]
    pub architectures: Vec<String>,
    #[serde(deserialize_with = "deb822::line_delimited", rename = "SHA256")]
    pub files: Vec<FileEntry>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct SourcePackage {
    pub package: String,
    pub directory: String,
    #[serde(deserialize_with = "deb822::line_delimited")]
    pub files: Vec<FileEntry>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BinaryPackage {
    pub package: String,
    #[serde(deserialize_with = "deb822::de_from_str")]
    pub size: usize,
    pub filename: String,
}

#[derive(Clone, Debug)]
pub struct Repository {
    pub id: String,
    pub url: String,
}

fn lift_404<T>(result: Result<T>) -> Result<Option<T>> {
    match result {
        Ok(r) => Ok(Some(r)),
        Err(e) => {
            if is_http_client_error(&e) {
                Ok(None)
            } else {
                Err(e)
            }
        }
    }
}

async fn get_maybe_compressed_file(client: &Client, url: &str) -> Result<Bytes> {
    if let Some(bytes) =
        lift_404(client.get_compressed(format!("{}.gz", url), Compression::Gzip).await)?
    {
        return Ok(bytes);
    }

    if let Some(bytes) =
        lift_404(client.get_compressed(format!("{}.xz", url), Compression::Lzma).await)?
    {
        return Ok(bytes);
    }

    client.get(url).await
}

#[instrument(skip_all, fields(?url))]
async fn read_deb822_file<
    'client,
    'url,
    T: DeserializeOwned,
    Fut: Future<Output = Result<Bytes>>,
    F: FnOnce(&'client Client, &'url str) -> Fut,
>(
    download: F,
    client: &'client Client,
    url: &'url str,
) -> Result<Option<T>> {
    match lift_404(
        download(client, url)
            .await
            .with_context(|| format!("Failed to download {}", url)),
    )? {
        Some(bytes) => {
            let file = tokio::task::block_in_place(|| {
                rfc822_like::from_bytes(&bytes[..])
                    .with_context(|| format!("Failed to parse {}", url))
            })?;

            debug!("Retrieved repo file");
            Ok(Some(file))
        }

        None => Ok(None),
    }
}

impl Repository {
    pub async fn read_release(
        &self,
        client: &Client,
    ) -> Result<Option<RepositoryRelease>> {
        read_deb822_file(Client::get, client, &format!("{}/Release", &self.url)).await
    }

    pub async fn read_sources(
        &self,
        client: &Client,
        component: &str,
    ) -> Result<Option<Vec<SourcePackage>>> {
        read_deb822_file(
            get_maybe_compressed_file,
            client,
            &format!("{}/{}/source/Sources", &self.url, component),
        )
        .await
    }

    pub async fn read_packages(
        &self,
        client: &Client,
        component: &str,
        arch: &str,
    ) -> Result<Option<Vec<BinaryPackage>>> {
        read_deb822_file(
            get_maybe_compressed_file,
            client,
            &format!("{}/{}/binary-{}/Packages", &self.url, component, arch),
        )
        .await
    }

    pub async fn list_snapshots(&self, client: &Client) -> Result<Vec<Repository>> {
        // Snapshots are found by scraping the HTML listing pages. We can identity the
        // snapshot directories by names being formatting according to ISO 8601 date/time
        // format, with a slash at the end.
        static PATTERN: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"^\d{8}T\d{6}Z/$").unwrap());

        let bytes = client.get(format!("{}/snapshots/", self.url)).await?;
        let doc = std::str::from_utf8(&bytes[..])
            .context("Failed to convert snapshots to UTF-8")?;
        let html = Html::parse_document(doc);

        let mut snapshots = vec![];

        let selector = Selector::parse("td a").unwrap();
        for element in html.select(&selector) {
            if let Some(href) = element.value().attr("href") {
                if PATTERN.is_match(href) {
                    // SAFETY: This would not have matched the pattern if there were no
                    // trailing slash.
                    let id = href.strip_suffix('/').unwrap();
                    snapshots.push(Repository {
                        id: id.to_owned(),
                        url: format!("{}/snapshots/{}", self.url, id),
                    });
                }
            }
        }

        Ok(snapshots)
    }
}
