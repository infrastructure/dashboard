//! Contains utilities for parsing the RFC822 variant used in Debian package files.

use serde::{
    de::{IntoDeserializer, Visitor},
    Deserialize, Deserializer,
};
use std::{any::type_name, fmt::Display, marker::PhantomData, str::FromStr};

/// A space-delimited file entry (`HASH SIZE FILENAME`).
#[derive(Debug)]
pub struct FileEntry {
    pub hash: String,
    pub size: usize,
    pub filename: String,
}

impl<'de> Deserialize<'de> for FileEntry {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct FileEntryVisitor;

        impl<'de> Visitor<'de> for FileEntryVisitor {
            type Value = FileEntry;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("a file entry line")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                let parts: Vec<&str> = v.split_whitespace().collect();
                if parts.len() != 3 {
                    return Err(E::custom("invalid number of components in line"));
                }

                let size = match parts[1].parse() {
                    Ok(size) => size,
                    Err(_) => return Err(E::custom("invalid size value")),
                };

                Ok(FileEntry {
                    hash: parts[0].to_owned(),
                    size,
                    filename: parts[2].to_owned(),
                })
            }
        }

        deserializer.deserialize_string(FileEntryVisitor)
    }
}

fn delimited<'de, T, D>(deserializer: D, delim: char) -> Result<Vec<T>, D::Error>
where
    T: Deserialize<'de>,
    D: Deserializer<'de>,
{
    struct SplitVisitor<T> {
        delim: char,
        phantom: PhantomData<T>,
    }

    impl<'de, T> Visitor<'de> for SplitVisitor<T>
    where
        T: Deserialize<'de>,
    {
        type Value = Vec<T>;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("a string")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            let mut result = vec![];
            for part in v.trim().split(self.delim) {
                result.push(T::deserialize(part.into_deserializer())?);
            }
            Ok(result)
        }
    }

    deserializer.deserialize_string(SplitVisitor { delim, phantom: PhantomData })
}

/// Deserializes a newline-delimited string.
pub fn line_delimited<'de, T, D>(deserializer: D) -> Result<Vec<T>, D::Error>
where
    T: Deserialize<'de>,
    D: Deserializer<'de>,
{
    delimited(deserializer, '\n')
}

/// Deserializes a space-delimited string.
pub fn space_delimited<'de, T, D>(deserializer: D) -> Result<Vec<T>, D::Error>
where
    T: Deserialize<'de>,
    D: Deserializer<'de>,
{
    delimited(deserializer, ' ')
}

/// Deserializes a string value using `FromStr`.
pub fn de_from_str<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: FromStr,
    T::Err: Display,
    D: Deserializer<'de>,
{
    struct FromStrVisitor<T: FromStr>(PhantomData<T>);

    impl<'de, T> Visitor<'de> for FromStrVisitor<T>
    where
        T: FromStr,
        T::Err: Display,
    {
        type Value = T;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("a ")?;
            formatter.write_str(type_name::<T>())
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            T::from_str(v).map_err(E::custom)
        }
    }

    deserializer.deserialize_string(FromStrVisitor(PhantomData))
}
