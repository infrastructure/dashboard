mod client;
mod deb822;
mod repository;
mod sources;
mod stats;
mod usage;

use crate::{
    client::{Client, ClientOptions},
    sources::SourceDefinition,
    stats::SuiteStats,
};
use anyhow::{anyhow, Context, Result};
use argh::FromArgs;
use netrc::Netrc;
use parse_display::FromStr;
use serde_derive::Serialize;
use std::{
    fs::File,
    io::{self, BufReader},
    path::{Path, PathBuf},
};
use tracing::{debug, info};

#[derive(FromStr)]
#[display(style = "snake_case")]
enum LogLevel {
    Quiet,
    Info,
    Debug,
    Trace,
}

#[derive(FromArgs)]
/// Gathers statistics for the storage usage of the current apt repos.
struct StorageStats {
    /// the YAML file defining the sources to be retrieved
    #[argh(option)]
    sources_definitions: PathBuf,
    /// the YAML key with the sources to be retrieved
    #[argh(option)]
    sources_key: String,
    /// file to store results in YAML format
    #[argh(option)]
    yaml: PathBuf,
    /// max number of snapshots to scan (useful for testing)
    #[argh(option)]
    max_snapshots: Option<usize>,
    /// max number of parallel network requests to send
    #[argh(option, default = "6")]
    max_parallel_requests: usize,
    /// set the logging level
    #[argh(option, default = "LogLevel::Info")]
    log_level: LogLevel,
    /// load credentials from the given .netrc (~/.netrc will be used by default, if it
    /// exists)
    #[argh(option)]
    netrc: Option<PathBuf>,
}

fn load_netrc<P: AsRef<Path>>(path: P) -> Result<Netrc> {
    let path = path.as_ref();

    let file =
        File::open(path).with_context(|| format!("Failed to open {}", path.display()))?;
    let reader = BufReader::new(file);

    Netrc::parse(reader)
        .map_err(|e| match e {
            netrc::Error::Io(io) => io.into(),
            netrc::Error::Parse(message, _) => anyhow!("{}", message),
        })
        .with_context(|| format!("Failed to parse {}", path.display()))
}

#[derive(Serialize)]
struct Results {
    storage_stats: Vec<SuiteStats>,
}

fn save_stats(dest: &Path, results: Results) -> Result<()> {
    let file = File::create(dest)
        .with_context(|| format!("Failed to open output file {}", dest.display()))?;
    serde_yaml::to_writer(file, &results).context("Failed to save stats")?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let args: StorageStats = argh::from_env();

    if std::env::var("RUST_LOG").is_err() {
        let trace_level = match args.log_level {
            LogLevel::Quiet => "warn",
            LogLevel::Info => "info",
            LogLevel::Debug => "debug",
            LogLevel::Trace => "trace",
        };

        std::env::set_var("RUST_LOG", format!("storage_stats={}", trace_level));
    }

    tracing_subscriber::fmt::init();

    let netrc = if let Some(netrc_path) = args.netrc {
        Some(load_netrc(netrc_path)?)
    } else if let Ok(home) = std::env::var("HOME") {
        let default_netrc_path = PathBuf::from(home).join(".netrc");
        match load_netrc(&default_netrc_path) {
            // If it wasn't found, it's not an error.
            Err(err)
                if err
                    .downcast_ref::<io::Error>()
                    .map(|err| err.kind() == io::ErrorKind::NotFound)
                    .unwrap_or(false) =>
            {
                debug!(?default_netrc_path, "Default netrc not found");
                None
            }
            result => Some(result?),
        }
    } else {
        None
    };

    let client =
        Client::new(ClientOptions { max_parallel: args.max_parallel_requests, netrc });
    let source_defs: Vec<_> =
        SourceDefinition::load(&args.sources_definitions, &args.sources_key)?
            .into_iter()
            .map(|(_, def)| def)
            .collect();

    let all_stats =
        SuiteStats::compute_all(&client, &source_defs, args.max_snapshots).await?;
    save_stats(&args.yaml, Results { storage_stats: all_stats })?;

    info!("Done!");
    Ok(())
}
