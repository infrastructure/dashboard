//! Parses the `channels/sources.yaml` files listing Debian repositories to connect to.

use crate::repository::Repository;
use anyhow::{Context, Result};
use dynfmt::{Format, SimpleCurlyFormat};
use serde::{de::Error as SerdeError, Deserialize, Deserializer};
use serde_derive::Deserialize;
use std::{collections::HashMap, fs::File, path::Path};

#[derive(Debug)]
pub struct SourceDefinition {
    pub suite: String,
    pub url: String,
    pub components: Vec<String>,
}

impl SourceDefinition {
    pub fn root_repository(&self) -> Repository {
        Repository { id: self.suite.clone(), url: self.url.clone() }
    }
}

// Custom deserializer to expand the URL template string.
impl<'de> Deserialize<'de> for SourceDefinition {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Debug, Deserialize)]
        struct Template {
            suite: String,
            url_template: String,
            components: Vec<String>,
        }

        let template = Template::deserialize(deserializer)?;

        let mut named_args: HashMap<&str, &str> = HashMap::new();
        named_args.insert("suite", &template.suite);

        match SimpleCurlyFormat.format(&template.url_template, &named_args) {
            Ok(url) => Ok(SourceDefinition {
                suite: template.suite,
                url: url.into_owned(),
                components: template.components,
            }),
            Err(e) => Err(D::Error::custom(e.to_string())),
        }
    }
}

impl SourceDefinition {
    pub fn load(
        path: &Path,
        sources_key: &str,
    ) -> Result<HashMap<String, SourceDefinition>> {
        let file = File::open(path)
            .with_context(|| format!("Failed to open {}", path.display()))?;
        let yaml: serde_yaml::Mapping = serde_yaml::from_reader(file)
            .with_context(|| format!("Failed to parse {}", path.display()))?;

        let defs = yaml
            .get(&serde_yaml::Value::String(sources_key.to_owned()))
            .with_context(|| format!("No key '{}' in {}", sources_key, path.display()))?;

        // serde_yaml doesn't do anchors, so a separate pass needs to be run on the
        // `Value` before finishing deserialization.
        let defs =
            yaml_merge_keys::merge_keys_serde(defs.to_owned()).with_context(|| {
                format!("Failed to merge '{}' in {}", sources_key, path.display())
            })?;

        serde_yaml::from_value(defs).with_context(|| {
            format!("Failed to load '{}' in {}", sources_key, path.display())
        })
    }
}
