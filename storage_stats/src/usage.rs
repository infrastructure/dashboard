//! Aggregates storage usage across multiple storage locations.

use dashmap::DashMap;
use parking_lot::Mutex;
use serde_derive::Serialize;
use std::{collections::BTreeMap, sync::Arc};
use tracing::{instrument, trace};

#[derive(Debug)]
struct StorageFile {
    first_key: String,
    last_key: String,
    size: usize,
}

/// Represents the usage of a single storage location. History is measured as the amount
/// of space that would be reclaimed if all storage locations older than the given one are
/// deleted (this assumes that storage keys are ordered by date).
#[derive(Debug, Serialize)]
pub struct StorageUsage {
    pub direct: usize,
    pub unique: usize,
    pub history: usize,
}

pub type StorageUsageResults = BTreeMap<String, StorageUsage>;

pub struct AggregationOptions {
    pub compute_history: bool,
}

pub struct StorageUsageAggregator {
    initial_key_sizes: Mutex<BTreeMap<String, usize>>,
    files: DashMap<String, StorageFile>,
}

impl StorageUsageAggregator {
    pub fn new() -> StorageUsageAggregator {
        StorageUsageAggregator {
            files: DashMap::new(),
            initial_key_sizes: Mutex::new(BTreeMap::new()),
        }
    }

    /// Registers a new key with the given initial size.
    pub fn register_key(&self, key: String, initial_size: usize) {
        let prev_value = self.initial_key_sizes.lock().insert(key.clone(), initial_size);
        assert!(prev_value.is_none(), "Duplicate key insert: {}", key);
    }

    /// Adds a file to the storage identified by the given key.
    #[instrument(skip(self))]
    pub fn add(&self, key: &str, path: String, size: usize) {
        trace!("Add file");

        self.files
            .entry(path)
            .and_modify(|file| {
                assert_eq!(file.size, size);

                if key < file.first_key.as_str() {
                    file.first_key = key.to_owned();
                } else if key > file.last_key.as_str() {
                    file.last_key = key.to_owned();
                }
            })
            .or_insert_with(|| StorageFile {
                first_key: key.to_owned(),
                last_key: key.to_owned(),
                size,
            });
    }

    pub fn collect(self, options: AggregationOptions) -> StorageUsageResults {
        let mut results = StorageUsageResults::new();
        let mut initial_history = 0;
        for (key, initial_size) in &*self.initial_key_sizes.lock() {
            results.insert(
                key.clone(),
                StorageUsage {
                    direct: *initial_size,
                    unique: *initial_size,
                    history: initial_history,
                },
            );

            if options.compute_history {
                initial_history += initial_size;
            }
        }

        for (path, file) in self.files.into_iter() {
            // First, we add the file's size to the total storage for all storage keys it
            // was sighted in.
            let mut it = results.range_mut(file.first_key.clone()..);
            for (key, storage) in &mut it {
                storage.direct += file.size;

                // If this is the last key it was seen in, exit out now so that the
                // history computation can take place right afterwards...
                if *key == file.last_key {
                    // ...but first, if the first and last keys are the same, then the
                    // file was *only* found in that storage location, so add it to its
                    // unique storage used.
                    if file.first_key == file.last_key {
                        trace!(?path, ?file, "Found unique");
                        storage.unique += file.size;
                    } else {
                        trace!(?path, ?file, "Not unique");
                    }

                    break;
                } else {
                    assert!(
                        *key < file.last_key,
                        "Unknown key: {:?} from {}",
                        file,
                        path
                    );
                }
            }

            if options.compute_history {
                // This file was removed after its last key, so add its size to the
                // history of all the keys that follow the last one.
                for (_, subsequent_storage) in it {
                    subsequent_storage.history += file.size;
                }
            }
        }

        results
    }

    pub fn unwrap_collect(
        self: Arc<StorageUsageAggregator>,
        options: AggregationOptions,
    ) -> StorageUsageResults {
        match Arc::try_unwrap(self) {
            Ok(inner) => inner.collect(options),
            Err(_) => panic!("StorageUsageAggregator was still in use"),
        }
    }
}
