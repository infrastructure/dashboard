# Dashboard

The pipeline here collects data from all the services (GitLab, OBS, APT) and
from upstreams (Debian, or Apertis itself for downstreams) and aggregates the
information in a dashboard flagging any incosistency and reporting available
updated that can be pulled from upstreams.

## Credentials

The dashboard pipeline requires special permissions to access repositories and
to trigger pipelines, the variable `DASHBOARD_GITLAB_API_TOKEN` must be defined
with an [access token for the `pkg/` group](https://gitlab.apertis.org/groups/pkg/-/settings/access_tokens)
with the following parameters:

- `Token name`: `dashboard`
- `Role`: `Maintainer`
- `Scope`: `api`

## Usage

The pipeline runs automatically on a regular schedule.

In addition it accepts some extra variables when being triggered manually:

* `DEBUG`: turn on verbose output
* `TRIGGER_UPDATES`: do not just report packaging updates, but also trigger
  them in the respective packaging repositories

See the description for each variable in the GitLab UI when triggering the
pipeline manually for more details.

# Development

Running the whole pipeline locally during development is quite cumbersome and
the `localtest` script is meant to help with that.

In particular it allows developers to skip jobs and to narrow the retrieved
information to a subset of the packages, massively cutting down the time needed
to run everything from scratch:

    ./localtest --skip-job=storage-usage --filter-packages=apache-log4j2 | sh

## License

MPL-2.0
